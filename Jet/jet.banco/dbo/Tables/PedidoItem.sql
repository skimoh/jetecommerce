﻿CREATE TABLE [dbo].[PedidoItem] (
    [IdPedidoItem] INT             IDENTITY (1, 1) NOT NULL,
    [IdPedido]     INT             NOT NULL,
    [IdProduto]    INT             NOT NULL,
    [Quantidade]   INT             NOT NULL,
    [Valor]        DECIMAL (10, 2) NOT NULL,
    CONSTRAINT [PK_PedidoItem] PRIMARY KEY CLUSTERED ([IdPedidoItem] ASC),
    CONSTRAINT [FK_PedidoItem_Pedido] FOREIGN KEY ([IdPedido]) REFERENCES [dbo].[Pedido] ([IdPedido]),
    CONSTRAINT [FK_PedidoItem_Produto] FOREIGN KEY ([IdProduto]) REFERENCES [dbo].[Produto] ([IdProduto])
);

