﻿CREATE TABLE [dbo].[Produto] (
    [IdProduto] INT             IDENTITY (1, 1) NOT NULL,
    [Nome]      VARCHAR (50)    NOT NULL,
    [Preco]     DECIMAL (10, 2) NOT NULL,
    [UrlImagem] VARCHAR (150)   NOT NULL,
    CONSTRAINT [PK_Produto] PRIMARY KEY CLUSTERED ([IdProduto] ASC)
);

