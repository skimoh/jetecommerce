﻿CREATE TABLE [dbo].[Pedido] (
    [IdPedido] INT             IDENTITY (1, 1) NOT NULL,
    [Cliente]  VARCHAR (50)    NOT NULL,
    [Endereco] VARCHAR (200)   NOT NULL,
    [Total]    DECIMAL (10, 2) NOT NULL,
    CONSTRAINT [PK_Pedido] PRIMARY KEY CLUSTERED ([IdPedido] ASC)
);

