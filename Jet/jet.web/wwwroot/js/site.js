﻿$(document).ready(function () {

    var arr = [];
    var local = localStorage.getItem("sacola");
    if (local != undefined) {
        arr = JSON.parse(localStorage.getItem("sacola"));
    }
    $('#totalItem').text(arr.length);

    if (_home) {
        Invoke('Pedido', 'GET', null, fnRetorno)
    }

    if (_sacola) {

        var total = 0;
        for (var i = 0; i < arr.length; i++) {

            var bloco = '<tr><td>' + arr[i].nome + '</td><td><input class="txtQtde" data-id="' + arr[i].id + '" type="number" value="' + arr[i].quantidade + '" /></td><td>R$ ' + arr[i].preco.toFixed(2) + '</td></tr >';
            total += parseFloat(arr[i].preco) * parseInt(arr[i].quantidade);
            $('#tr-itens').append(bloco);
        }
        $('#td-total').text('R$ ' + total.toFixed(2))
        $('#hfTotal').val(total.toFixed(2));
    }


    $('.txtQtde').blur(function (e) {

        var id = $(this).data('id')

        var local = localStorage.getItem("sacola");
        if (local != undefined) {
            arr = JSON.parse(localStorage.getItem("sacola"));

            var obj = arr.find(x => x.id === id);
            if (obj != null) {
                obj.quantidade = $(this).val();

                arr.splice(arr.findIndex(v => v.id == id), 1);
            } else {
                obj = produto;
            }

            arr.push(obj);
            localStorage.setItem("sacola", JSON.stringify(arr));

            location.reload();
        }
    });

    $('#btnFinalizar').click(function (e) {
        debugger
        var itens = [];

        var local = localStorage.getItem("sacola");
        if (local != undefined) {
            var arr = JSON.parse(localStorage.getItem("sacola"));
            for (var i = 0; i < arr.length; i++) {
                var obj = {
                    "IdPedidoItem": 0,
                    "IdProduto": arr[i].id,
                    "Quantidade": arr[i].quantidade,
                    "Valor": arr[i].preco
                }
                itens.push(obj)
            }
        }

        var pedido = {
            "IdPedido": 0,
            "Cliente": $('#txtCliente').val(),
            "Endereco": $('#txtEndereco').val(),
            "Total": $('#hfTotal').val(),
            "PedidoItem": itens
        };

        Invoke('Pedido', 'POST', pedido, fnRetornoPedido)

    });

});

function fnRetornoPedido(resp) {
    fnMensagem('Pedido realizado com sucesso => ' + resp);
}
function fnRetorno(resp) {

    $('.catalogo').empty();

    for (var i = 0; i < resp.length; i++) {
        var bloco = '<div class="col-lg-3 col-md-6 mb-4"><div class="card h-100">';
        bloco = bloco + '<img class="card-img-top" src="' + resp[i].urlImagem + '" width="500" height="325" alt="">';
        bloco = bloco + '<div class="card-body"><h4 class="card-title">R$ ' + parseFloat(resp[i].preco).toFixed(2).replace('.', ',') + '</h4>';
        bloco = bloco + '<p class="card-text">' + resp[i].nome + '</p></div><div class="card-footer">';
        bloco = bloco + '<button onclick="Adicionar(' + resp[i].idProduto + ',' + resp[i].preco + ',\'' + resp[i].nome + '\')" ; class="btn btn-dark">Adicionar</button></div></div></div>';

        $('.catalogo').append(bloco);
    }

}

function Adicionar(id, preco, nome) {
    var arr = [];
    var produto = {
        "id": id,
        "preco": preco,
        "nome": nome,
        "quantidade": 1
    }

    var local = localStorage.getItem("sacola");
    if (local != undefined) {
        arr = JSON.parse(localStorage.getItem("sacola"));

        var obj = arr.find(x => x.id === produto.id);
        if (obj != null) {
            obj.quantidade++;

            arr.splice(arr.findIndex(v => v.id == produto.id), 1);
        } else {
            obj = produto;
        }

        arr.push(obj);
        localStorage.setItem("sacola", JSON.stringify(arr));
    }
    else {
        var arrNovo = [produto];
        localStorage.setItem("sacola", JSON.stringify(arrNovo));
        arr = arrNovo;
    }
    $('#totalItem').text(arr.length);
    fnMensagem('Produto adicionado a Sacola')
}


function Invoke(url, method, param, handleData) {

    $.ajax({

        url: _host + url,
        type: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        //dataType: "json",
        //contentType: "application/json;charset=utf-8",
        data: JSON.stringify(param),
        success: handleData
    }).fail(function (jqXHR, textStatus, msg) {
        fnErro(msg);
    });
}

$body = $("body");
$(document).on({
    ajaxStart: function () { $body.addClass("loading"); },
    ajaxStop: function () { $body.removeClass("loading"); }
});

$(window).on('load', function () {
    $(".loaderPage").fadeOut("slow");
});


function fnMensagem(msg) {
    swal("Sucesso!", msg, "success");
}

function fnSucesso() {
    swal("Sucesso!", "Operação realizada com sucesso!", "success");
}

function fnErro(erro) {
    swal("Erro!", erro, "error");
}