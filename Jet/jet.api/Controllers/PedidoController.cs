﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace jet.api.Controllers
{
    /// <summary>
    /// classe pedido
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class PedidoController : ControllerBase
    {

        private readonly ILogger<PedidoController> _logger;

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="logger"></param>
        public PedidoController(ILogger<PedidoController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Produto> Get()
        {
            using (var db = new BancoContext())
            {
                return db.Produto.OrderBy(b => b.Nome).AsNoTracking().ToList();
            }
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]                
        public int Post([FromBody] Pedido req)
        {
            try
            {
                using (var db = new BancoContext())
                {
                    db.Add(req);
                    db.SaveChanges();

                    return req.IdPedido;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Erro ao persistir o pedido", req);
                return 1;
            }            
        }
    }
}
