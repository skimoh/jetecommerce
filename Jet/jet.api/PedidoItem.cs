using System;
using System.ComponentModel.DataAnnotations;

namespace jet.api
{
    public class PedidoItem
    {
        [Key]        
        public int IdPedidoItem { get; set; }

        public int IdPedido { get; set; }

        public int IdProduto { get; set; }

        public int Quantidade { get; set; }

        public decimal Valor { get; set; }

        public virtual Pedido Pedido { get; set; }

        public virtual Produto Produto { get; set; }
    }
}
