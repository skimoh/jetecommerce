using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace jet.api
{
    public class Pedido
    {
        public Pedido()
        {
            PedidoItem = new HashSet<PedidoItem>();
        }

        [Key]
        public int IdPedido { get; set; }

        [Required]
        [StringLength(50)]
        public string Cliente { get; set; }

        [Required]
        [StringLength(200)]
        public string Endereco { get; set; }

        public decimal Total { get; set; }

        public virtual ICollection<PedidoItem> PedidoItem { get; set; }
    }
}
