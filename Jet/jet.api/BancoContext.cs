﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jet.api
{
    public class BancoContext : DbContext
    {
        public virtual DbSet<Pedido> Pedido { get; set; }
        public virtual DbSet<PedidoItem> PedidoItem { get; set; }
        public virtual DbSet<Produto> Produto { get; set; }

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=DESKTOP-8URI4NI\SQLEXPRESS19;initial catalog=Banco;user id=sa;password=123456;MultipleActiveResultSets=True;App=EntityFramework;Enlist=False");
        }
    }
}
