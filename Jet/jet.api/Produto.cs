using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace jet.api
{
    public class Produto
    {
        public Produto()
        {
            PedidoItem = new HashSet<PedidoItem>();
        }

        [Key]
        public int IdProduto { get; set; }

        [Required]
        [StringLength(50)]
        public string Nome { get; set; }

        public decimal Preco { get; set; }

        [Required]
        [StringLength(150)]
        public string UrlImagem { get; set; }

        public virtual ICollection<PedidoItem> PedidoItem { get; set; }
    }
}
